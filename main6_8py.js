var main6_8py =
[
    [ "Ch1", "main6_8py.html#a731964570673ac9ddb7aafc9d1cce047", null ],
    [ "Ch2", "main6_8py.html#ae2687b53bf05626f8f84df91e058a83c", null ],
    [ "ClosedLoop", "main6_8py.html#a52ea8a54d8611ea1aae2da3b72ba278e", null ],
    [ "Encoder", "main6_8py.html#a37874ead0d6525ceae00620ab997ed84", null ],
    [ "interval", "main6_8py.html#a043e13c2b4071e17f25828234ddf9ec1", null ],
    [ "Motor", "main6_8py.html#a3a35ac389e94bb7f19b22da806411e1d", null ],
    [ "nSLEEP_pin", "main6_8py.html#a909b80d7565ebbee718dc12196c18796", null ],
    [ "PC6", "main6_8py.html#a67a51d6463c5f1be7fbed048d5e0a1ef", null ],
    [ "PC7", "main6_8py.html#af5b65bee8bb01c9c06458382e81443b8", null ],
    [ "pinB0", "main6_8py.html#aaab4aca8f6cd85c9548f130769b8bbb1", null ],
    [ "pinB1", "main6_8py.html#a6c1490cd5527382f0e65ded9cf2c39ae", null ],
    [ "task0", "main6_8py.html#a02e87db0ee0172641ef99ac4598e1aa1", null ],
    [ "task1", "main6_8py.html#ae7a0eaf19589ecb8480276f8e520a0d4", null ],
    [ "taskList", "main6_8py.html#aa0d4852a76eae3ad210815f5f89a73b5", null ],
    [ "tim", "main6_8py.html#a346a9c2e36898d14d7b6d26b9462ce9b", null ],
    [ "timer", "main6_8py.html#a15bc1b52c0fcc28b313a3556275eeb2e", null ]
];