var files_dup =
[
    [ "ClosedLoopModule6.py", "ClosedLoopModule6_8py.html", [
      [ "ClosedLoop", "classClosedLoopModule6_1_1ClosedLoop.html", "classClosedLoopModule6_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoopModule7.py", "ClosedLoopModule7_8py.html", [
      [ "ClosedLoop", "classClosedLoopModule7_1_1ClosedLoop.html", "classClosedLoopModule7_1_1ClosedLoop" ]
    ] ],
    [ "Controller6.py", "Controller6_8py.html", [
      [ "Controller", "classController6_1_1Controller.html", "classController6_1_1Controller" ]
    ] ],
    [ "Controller7.py", "Controller7_8py.html", [
      [ "Controller", "classController7_1_1Controller.html", "classController7_1_1Controller" ]
    ] ],
    [ "FibonacciCalc.py", "FibonacciCalc_8py.html", [
      [ "Fibonacci", "classFibonacciCalc_1_1Fibonacci.html", "classFibonacciCalc_1_1Fibonacci" ]
    ] ],
    [ "hw0x00TaskElevator.py", "hw0x00TaskElevator_8py.html", "hw0x00TaskElevator_8py" ],
    [ "Lab5classes.py", "Lab5classes_8py.html", [
      [ "BLEModuleDriver", "classLab5classes_1_1BLEModuleDriver.html", "classLab5classes_1_1BLEModuleDriver" ]
    ] ],
    [ "LEDController.py", "LEDController_8py.html", [
      [ "LEDController", "classLEDController_1_1LEDController.html", "classLEDController_1_1LEDController" ]
    ] ],
    [ "main2.py", "main2_8py.html", "main2_8py" ],
    [ "main3.py", "main3_8py.html", "main3_8py" ],
    [ "main4.py", "main4_8py.html", "main4_8py" ],
    [ "main5.py", "main5_8py.html", "main5_8py" ],
    [ "main6.py", "main6_8py.html", "main6_8py" ],
    [ "main7.py", "main7_8py.html", "main7_8py" ],
    [ "maintestUI4.py", "maintestUI4_8py.html", "maintestUI4_8py" ],
    [ "maintestUI6.py", "maintestUI6_8py.html", "maintestUI6_8py" ],
    [ "maintestUI7.py", "maintestUI7_8py.html", "maintestUI7_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "nucencoder.py", "nucencoder_8py.html", [
      [ "EncoderDriver", "classnucencoder_1_1EncoderDriver.html", "classnucencoder_1_1EncoderDriver" ],
      [ "EncoderTask", "classnucencoder_1_1EncoderTask.html", "classnucencoder_1_1EncoderTask" ]
    ] ],
    [ "nucleoclasseslab2.py", "nucleoclasseslab2_8py.html", "nucleoclasseslab2_8py" ],
    [ "shares3.py", "shares3_8py.html", "shares3_8py" ],
    [ "shares5.py", "shares5_8py.html", "shares5_8py" ],
    [ "shares6.py", "shares6_8py.html", "shares6_8py" ],
    [ "shares7.py", "shares7_8py.html", "shares7_8py" ],
    [ "TaskUser3.py", "TaskUser3_8py.html", [
      [ "TaskUser1", "classTaskUser3_1_1TaskUser1.html", "classTaskUser3_1_1TaskUser1" ]
    ] ],
    [ "TaskUser6.py", "TaskUser6_8py.html", [
      [ "Task_usr", "classTaskUser6_1_1Task__usr.html", "classTaskUser6_1_1Task__usr" ]
    ] ],
    [ "TaskUser7.py", "TaskUser7_8py.html", [
      [ "Task_usr", "classTaskUser7_1_1Task__usr.html", "classTaskUser7_1_1Task__usr" ]
    ] ],
    [ "UI_dataGen.py", "UI__dataGen_8py.html", [
      [ "UI_dataGenTask", "classUI__dataGen_1_1UI__dataGenTask.html", "classUI__dataGen_1_1UI__dataGenTask" ]
    ] ],
    [ "UI_FrontLab4.py", "UI__FrontLab4_8py.html", [
      [ "UI_Task", "classUI__FrontLab4_1_1UI__Task.html", "classUI__FrontLab4_1_1UI__Task" ]
    ] ],
    [ "UI_FrontLab6.py", "UI__FrontLab6_8py.html", [
      [ "UI_Task", "classUI__FrontLab6_1_1UI__Task.html", "classUI__FrontLab6_1_1UI__Task" ]
    ] ],
    [ "UI_FrontLab7.py", "UI__FrontLab7_8py.html", [
      [ "UI_Task", "classUI__FrontLab7_1_1UI__Task.html", "classUI__FrontLab7_1_1UI__Task" ]
    ] ]
];