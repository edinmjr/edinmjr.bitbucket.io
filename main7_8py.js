var main7_8py =
[
    [ "Ch1", "main7_8py.html#a37648d5a8b4ea022c519240fea570a36", null ],
    [ "Ch2", "main7_8py.html#a9c3c1ad4ce6b315446dd87aa46e77c7c", null ],
    [ "ClosedLoop", "main7_8py.html#a3a2f5ef92b39f2addaf306c228493e43", null ],
    [ "Encoder", "main7_8py.html#a74eb5ab0a5e40527f30a2a5c1afaf652", null ],
    [ "interval", "main7_8py.html#a9b290ccd1b63f483561a20d01c5daa73", null ],
    [ "Motor", "main7_8py.html#a8de99a1dc484c7230c45a97ede2f9672", null ],
    [ "nSLEEP_pin", "main7_8py.html#a6594329128f615637a7eaed1379dad9e", null ],
    [ "PC6", "main7_8py.html#aef278b18e068805574a64b496d99bce8", null ],
    [ "PC7", "main7_8py.html#a1d358a2f750cca8f4710ba2432e48693", null ],
    [ "pinB0", "main7_8py.html#af9beb889fb34a36c5c16e72af65dbb44", null ],
    [ "pinB1", "main7_8py.html#a22edcf4b8ab86cce96cb0f54d67f7936", null ],
    [ "task0", "main7_8py.html#a55c7aaafbac5844e705a387a7694ece5", null ],
    [ "task1", "main7_8py.html#a923e80dd9b83135dff4f50caf249e5f2", null ],
    [ "taskList", "main7_8py.html#aa82ffbcfa02fb72edecd0f3154639d2a", null ],
    [ "tim", "main7_8py.html#a88044fe493e12d9edb4311e80e446f4b", null ],
    [ "timer", "main7_8py.html#a062108a207c67da9016d39882248c424", null ]
];