var annotated_dup =
[
    [ "ClosedLoopModule6", "namespaceClosedLoopModule6.html", "namespaceClosedLoopModule6" ],
    [ "ClosedLoopModule7", null, [
      [ "ClosedLoop", "classClosedLoopModule7_1_1ClosedLoop.html", "classClosedLoopModule7_1_1ClosedLoop" ]
    ] ],
    [ "Controller6", null, [
      [ "Controller", "classController6_1_1Controller.html", "classController6_1_1Controller" ]
    ] ],
    [ "Controller7", "namespaceController7.html", "namespaceController7" ],
    [ "FibonacciCalc", "namespaceFibonacciCalc.html", "namespaceFibonacciCalc" ],
    [ "hw0x00TaskElevator", "namespacehw0x00TaskElevator.html", "namespacehw0x00TaskElevator" ],
    [ "Lab5classes", null, [
      [ "BLEModuleDriver", "classLab5classes_1_1BLEModuleDriver.html", "classLab5classes_1_1BLEModuleDriver" ]
    ] ],
    [ "LEDController", "namespaceLEDController.html", "namespaceLEDController" ],
    [ "MotorDriver", "namespaceMotorDriver.html", "namespaceMotorDriver" ],
    [ "nucencoder", "namespacenucencoder.html", "namespacenucencoder" ],
    [ "nucleoclasseslab2", "namespacenucleoclasseslab2.html", "namespacenucleoclasseslab2" ],
    [ "TaskUser3", "namespaceTaskUser3.html", "namespaceTaskUser3" ],
    [ "TaskUser6", null, [
      [ "Task_usr", "classTaskUser6_1_1Task__usr.html", "classTaskUser6_1_1Task__usr" ]
    ] ],
    [ "TaskUser7", "namespaceTaskUser7.html", "namespaceTaskUser7" ],
    [ "UI_dataGen", "namespaceUI__dataGen.html", "namespaceUI__dataGen" ],
    [ "UI_FrontLab4", "namespaceUI__FrontLab4.html", "namespaceUI__FrontLab4" ],
    [ "UI_FrontLab6", "namespaceUI__FrontLab6.html", "namespaceUI__FrontLab6" ],
    [ "UI_FrontLab7", "namespaceUI__FrontLab7.html", "namespaceUI__FrontLab7" ],
    [ "BLEModuleDriver", "classBLEModuleDriver.html", null ],
    [ "Encoder", "classEncoder.html", null ],
    [ "LEDController", "classLEDController.html", null ],
    [ "MotorDriver", "classMotorDriver.html", null ]
];