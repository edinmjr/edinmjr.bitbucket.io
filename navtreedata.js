/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME305 Lab Portfolio", "index.html", [
    [ "Portfolio Overview", "index.html#sec_port", null ],
    [ "Lab 0", "page_lab0.html", null ],
    [ "Fibonacci Calculator", "page_fib.html", null ],
    [ "ElevatorFSM", "page_elevator.html", [
      [ "ElevatorFSM Source Code Access", "page_elevator.html#page_elevator", null ]
    ] ],
    [ "Lab 2", "page_lab2.html", [
      [ "Nucleo blinking LED Source Code Access", "page_lab2.html#page_lab2", null ]
    ] ],
    [ "Nucleo Encoder", "page_Nucleo.html", [
      [ "Nucleo_Encoder Source Code Access", "page_Nucleo.html#page_Nucleo", null ]
    ] ],
    [ "UI Data Generator", "page_Lab4.html", [
      [ "Data Generator Source Code Access", "page_Lab4.html#page_Lab4", null ]
    ] ],
    [ "Bluetooth LED Control", "page_LEDBLE.html", [
      [ "Bluetooth LED Control Source Code Access", "page_LEDBLE.html#page_LEDBLE", null ],
      [ "Documentation", "page_LEDBLE.html#page_LEDBLE_doc", null ],
      [ "Instructions", "page_LEDBLE.html#page_LEDBLE_Instructions", null ]
    ] ],
    [ "Closed Loop Speed Controller", "page_Lab6.html", [
      [ "Closed Loop Speed Controller Source Code Access", "page_Lab6.html#page_Lab6", null ],
      [ "Instructions", "page_Lab6.html#page_Lab6_Instructions", null ]
    ] ],
    [ "Reference Velocity Profile Tracker", "page_Lab7.html", [
      [ "Reference Velocity Profile Tracker Source Code Access", "page_Lab7.html#page_Lab7", null ],
      [ "Instructions", "page_Lab7.html#page_Lab7_Instructions", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ClosedLoopModule6_8py.html",
"classUI__FrontLab6_1_1UI__Task.html#afc857400e45750bd54f320e512b478e7",
"main6_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';