var searchData=
[
  ['ch1_10',['Ch1',['../namespacemain3.html#a60be645549128dff3a0fee1d8a5827d5',1,'main3.Ch1()'],['../namespacemain4.html#a89c4f3f3a3f0a3cf9b7b57079870095e',1,'main4.Ch1()']]],
  ['ch2_11',['Ch2',['../namespacemain3.html#a5ada717c48b4b2bf2805dd25cb186fea',1,'main3.Ch2()'],['../namespacemain4.html#a5222c43b60adf5785c5a0ccc2889a5a9',1,'main4.Ch2()']]],
  ['check_12',['check',['../classLab5classes_1_1BLEModuleDriver.html#a95c79ead759d2c229dbbc20290bcb4f2',1,'Lab5classes::BLEModuleDriver']]],
  ['checkspeed_13',['checkSpeed',['../classController6_1_1Controller.html#a697635ed56b7b05bde8caec860466794',1,'Controller6::Controller']]],
  ['closedloop_14',['ClosedLoop',['../classClosedLoopModule7_1_1ClosedLoop.html',1,'ClosedLoopModule7.ClosedLoop'],['../classClosedLoopModule6_1_1ClosedLoop.html',1,'ClosedLoopModule6.ClosedLoop'],['../classController6_1_1Controller.html#ad7f937d710d88384791a20a01ae92544',1,'Controller6.Controller.ClosedLoop()'],['../classController7_1_1Controller.html#adfe3957f5ba3169827e2c58fc3619d57',1,'Controller7.Controller.ClosedLoop()'],['../main6_8py.html#a52ea8a54d8611ea1aae2da3b72ba278e',1,'main6.ClosedLoop()'],['../namespacemain7.html#a3a2f5ef92b39f2addaf306c228493e43',1,'main7.ClosedLoop()']]],
  ['closedloopmodule6_15',['ClosedLoopModule6',['../namespaceClosedLoopModule6.html',1,'']]],
  ['closedloopmodule6_2epy_16',['ClosedLoopModule6.py',['../ClosedLoopModule6_8py.html',1,'']]],
  ['closedloopmodule7_2epy_17',['ClosedLoopModule7.py',['../ClosedLoopModule7_8py.html',1,'']]],
  ['cmd_18',['cmd',['../classLab5classes_1_1BLEModuleDriver.html#afe064e66c3c5039b849991c233caa703',1,'Lab5classes.BLEModuleDriver.cmd()'],['../classLEDController_1_1LEDController.html#a4a220cec4ad381ec690a8c7bcb5977bd',1,'LEDController.LEDController.cmd()'],['../classUI__dataGen_1_1UI__dataGenTask.html#a66849458bcefa2556142dfbcb9fe3db0',1,'UI_dataGen.UI_dataGenTask.cmd()'],['../classUI__FrontLab4_1_1UI__Task.html#a907fd163b06401ba72d6e6b8008e5cee',1,'UI_FrontLab4.UI_Task.cmd()'],['../classUI__FrontLab6_1_1UI__Task.html#ac58cf364a46374026445df4b1606b6b2',1,'UI_FrontLab6.UI_Task.cmd()'],['../shares3_8py.html#abf82d310a7c4c70d04c07213c0696c5c',1,'shares3.cmd()'],['../namespaceshares5.html#ae840842274fbc2f2132a9e6b7a006d54',1,'shares5.cmd()']]],
  ['controller_19',['Controller',['../classController7_1_1Controller.html',1,'Controller7.Controller'],['../classController6_1_1Controller.html',1,'Controller6.Controller']]],
  ['controller6_2epy_20',['Controller6.py',['../Controller6_8py.html',1,'']]],
  ['controller7_21',['Controller7',['../namespaceController7.html',1,'']]],
  ['controller7_2epy_22',['Controller7.py',['../Controller7_8py.html',1,'']]],
  ['converter_23',['converter',['../classnucencoder_1_1EncoderDriver.html#a9107665182e8aa4f9037c3bf2709e081',1,'nucencoder::EncoderDriver']]],
  ['count_24',['count',['../classnucencoder_1_1EncoderDriver.html#aa02c7d7edf2acc97588c84886d934f69',1,'nucencoder::EncoderDriver']]],
  ['curr_5ftime_25',['curr_time',['../classLab5classes_1_1BLEModuleDriver.html#ac0d88f45613e7f778028eefb663e49f9',1,'Lab5classes.BLEModuleDriver.curr_time()'],['../classLEDController_1_1LEDController.html#aaf9f74ce769b66f2cf9f1b474e72e061',1,'LEDController.LEDController.curr_time()'],['../classnucleoclasseslab2_1_1rLEDFSM.html#a3d8e49a72e919e765fa16ac04ce4ebc6',1,'nucleoclasseslab2.rLEDFSM.curr_time()'],['../classTaskUser3_1_1TaskUser1.html#addecc0f4743f1ce0eb99d382ea0ffe87',1,'TaskUser3.TaskUser1.curr_time()'],['../classUI__FrontLab4_1_1UI__Task.html#a20b2e40d180a50ea919d426579b8ccc0',1,'UI_FrontLab4.UI_Task.curr_time()'],['../classUI__FrontLab6_1_1UI__Task.html#a3d9928fc60f2ed30f7578f4031468298',1,'UI_FrontLab6.UI_Task.curr_time()'],['../classUI__FrontLab7_1_1UI__Task.html#a7d345461b0a466666c80e0e96f2b3ac5',1,'UI_FrontLab7.UI_Task.curr_time()']]],
  ['closed_20loop_20speed_20controller_26',['Closed Loop Speed Controller',['../page_Lab6.html',1,'']]]
];
