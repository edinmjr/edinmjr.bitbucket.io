var searchData=
[
  ['g_47',['g',['../classUI__FrontLab6_1_1UI__Task.html#ac03354211a57331d652ec1c625c9b254',1,'UI_FrontLab6.UI_Task.g()'],['../classUI__FrontLab7_1_1UI__Task.html#a11dc930597fa73a68b498a170ecefee5',1,'UI_FrontLab7.UI_Task.g()']]],
  ['gdelta_48',['gdelta',['../classnucencoder_1_1EncoderDriver.html#acba857bb970c2f7fa689294f295af56f',1,'nucencoder::EncoderDriver']]],
  ['get_5fkp_49',['get_Kp',['../classClosedLoopModule6_1_1ClosedLoop.html#aa067c68af293ae87859fa2bb0273e42c',1,'ClosedLoopModule6.ClosedLoop.get_Kp()'],['../classClosedLoopModule7_1_1ClosedLoop.html#a7b9f1fecca91fa68ed709a60e683dae0',1,'ClosedLoopModule7.ClosedLoop.get_Kp()']]],
  ['getangle_50',['getAngle',['../classnucencoder_1_1EncoderDriver.html#a3c2bdb537a6711a25f908e686e898f5c',1,'nucencoder::EncoderDriver']]],
  ['getbuttonstate_51',['getButtonState',['../classhw0x00TaskElevator_1_1Button.html#a7bcf444ba9426672e11e1527eb1be1b7',1,'hw0x00TaskElevator::Button']]],
  ['getdelta_52',['getDelta',['../classnucencoder_1_1EncoderDriver.html#a6f27625737eb9841c6dc8aed69147664',1,'nucencoder::EncoderDriver']]],
  ['getposition_53',['getPosition',['../classnucencoder_1_1EncoderDriver.html#a014f368995e1a2f6ac5ad8ecec54cad9',1,'nucencoder::EncoderDriver']]]
];
